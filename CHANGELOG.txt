2006-06-08
  * Converted to Drupal 4.7
  ** Forms API setting spage
  *** Thanks to 'hutch' for initial 4.7 forms API conversion
  **** http://drupal.org/user/52366
  ** Forms API validate and Submit
  ** Removed 4.6 specific patch for maintaining workflow flags. Not needed for 4.7
  ** All other 4.6 --> 4.7 conversion : http://drupal.org/node/22218
  ** Disabled Workflow action until phase 2
