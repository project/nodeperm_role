********************************************************************
                      D R U P A L    M O D U L E
********************************************************************
Name: nodeperm_role module
Current Maintainer: Robb Canfield
Original Author: Matt Westgate <drupal at asitis dot org>
Last update: (See CHANGELOG.txt for details)
Drupal: 4.7
Dependencies: node.module
********************************************************************
DESCRIPTION:

Allows users to assign role-based viewing and editing permissions for
nodes. This module is different from other node permissions modules
in three ways:

   1) If you only need to manage who can edit content (not
      who can view pages that are in the published state), you
      can disable and hide the view permissions component of this
      module and gain a performance boost.

   2) Roles can be restricted from editing their own node
      permissions.

   3) Use this module in conjunction with both Actions module
      (http://drupal.org/project/actions) and Workflow module
      (http://drupal.org/project/workflow) to dynamically alter
      node permissions when a node changes state (for example, you
      could give write access to users in the Reviewer role when a
      node changes workflow from 'draft' to 'review'). See
      INSTALL.txt for configuration details.

      !!!Not yet supported!!!

********************************************************************
INSTALLATION:

see the INSTALL.txt file in this directory.
